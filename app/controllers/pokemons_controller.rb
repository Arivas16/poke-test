class PokemonsController < ApplicationController

  def index
    # function loads all pokemons of national region because the 
    # pokedex national ID is 1
    begin
      url = Rails.configuration.api_url+'pokedex/1'
      response = RestClient.get url
      if response.code == 200
        # if response was successfully parse JSON
        response = JSON.parse(response)
        # get the values of all pokemons in the national region and paginate
        @pokemons = response["pokemon_entries"].paginate(:page => params[:page], :per_page => 12)
      else
        # if response code isn't succed, set a message to the user and show the view
        flash[:error] = 'Ocurrió un error en la consulta hacia pokeapi. Por favor, inténtalo nuevamente'
      end
    rescue
      #if an exception appears, set a message to the user and show the view
      flash[:error] = 'Ocurrió un error en la consulta hacia pokeapi. Por favor, inténtalo nuevamente'
    end
  end

  def show
    # function load the details of the pokemon with its
    # entry number, show the image, name, weight, height,
    # moves, abilities and species
    begin
      if params[:id].present?
        # get the details of pokemon given with its entry number
        url = Rails.configuration.api_url+'pokemon/'+params[:id]
        response = RestClient.get url
        if response.code == 200
          #if response was successfully parse JSON
          @pokemon = JSON.parse(response)
          #get the pokemon's moves in a single var to handle in the view
          @moves = @pokemon["moves"]
          #get the pokemon's abilities in a single var to handle in the view
          @abilities = @pokemon["abilities"]
          #get the pokemon's species in a single var to handle in the view
          @species = @pokemon["species"]
          #get the pokemon's images in a single var to handle in the view
          @sprites = @pokemon["sprites"]
        else
          # if response code isn't succed, set a message error to the user and redirect to index
          flash[:error] = 'Ocurrió un error en la consulta hacia pokeapi. Por favor, inténtalo nuevamente'
          redirect_to action: :index
        end
      else
        #if params ID is blank or nil, set a message error to the user and redirect to index
        flash[:error] = 'Faltan parámetros para la consulta hacia pokeapi. Por favor, inténtalo nuevamente'
        redirect_to action: :index
      end
    rescue
      #if an exception appears, set a message error to the user and redirect to index
      flash[:error] = 'Ocurrió un error en la consulta hacia pokeapi. Por favor, inténtalo nuevamente'
      redirect_to action: :index
    end
  end

end
